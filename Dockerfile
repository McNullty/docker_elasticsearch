FROM elasticsearch:latest

MAINTAINER Mladen Čikara <mladen.cikara@gmail.com>

RUN \
  /usr/share/elasticsearch/bin/plugin -install royrusso/elasticsearch-HQ && \
  /usr/share/elasticsearch/bin/plugin -install mobz/elasticsearch-head

CMD ["elasticsearch"]